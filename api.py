from flask import Flask, jsonify, request
from flask_cors import CORS
import json
import pymongo
from bson.objectid import ObjectId
from pymongo import MongoClient

app = Flask(__name__)
CORS(app)

cluster = MongoClient("mongodb+srv://root:root@cluster0.hn07h.mongodb.net/?retryWrites=true&w=majority")
db = cluster['store']
collection = db["store_flask"]

@app.route("/", methods=['GET', 'POST'])
def get_post():
    if request.method == 'GET':
        o = []
        for i in collection.find():
            o.append({'id': str(ObjectId(i["_id"])),'name' : i['name'], 'email' : i['email'], 'para' : i['para']})
        return jsonify(o)
    elif request.method == 'POST':
        id = collection.insert({"name" : request.json['name'], 'email' : request.json['email'], 'para': request.json['para']})
        return jsonify(str(ObjectId(id)))
        
@app.route("/<string:id>", methods=['PUT', 'DELETE'])
def put_del(id):
    if request.method == 'PUT':
        collection.update({ "_id": ObjectId(id)}, {
            "$set" : {
                "name" : request.json['name'],
                'email' : request.json['email'],
                'para': request.json['para']
            }
        })
        return jsonify({"message": "Updated!"})
    elif request.method == 'DELETE':
        collection.delete_one({"_id": ObjectId(id)})
        return jsonify({"message": "deleted!!"})

@app.route('/update/<string:id>', methods=['GET'])
def method_name(id):
    res = collection.find_one({"_id": ObjectId(id)})
    return {"id" : str(ObjectId(res["_id"])), "name": res['name'], "email": res['email'], "para": res['para']}


if __name__ == '__main__':
    app.run(debug=True)
            