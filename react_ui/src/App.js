import React, { Component } from 'react';
import { Container, Row, Col, Table, Button, Form, Card } from 'react-bootstrap';
import Navbars from './components/navbar'
import './App.css';
import axios from 'axios';

class App extends Component {

  constructor(props) {
    super(props); 
    this.state = {
      users : [],
      name : '',
      email : '',
      para : '',
      id : 0

    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/').then((res) => 
    this.setState({
      users : res.data,
      name : '',
      email : '',
      para : '',
      id : 0
    })
    
    )
  }

  nameChange = event => {
    this.setState({
      name : event.target.value
    })
  }

  emailChange = event => {
    this.setState({
      email : event.target.value
    })
  }

  paraChange = event => {
    this.setState({
      para : event.target.value
    })
  }

  submit(event, id) {
    event.preventDefault();
    if (id===0) {
      axios.post('http://localhost:5000/', {"name" : this.state.name, "email" : this.state.email, "para" : this.state.para })
      .then(() => {
        this.componentDidMount();
      })
    }
  }

  delete(id) {
    axios.delete(`http://localhost:5000/${id}`)
    .then(() => {
      this.componentDidMount();
    })
  }

  update(id) {
    axios.get(`http://localhost:5000/update/${id}`)
    .then((res) => {
      this.setState({
        name: res.data.name,
        email: res.data.email,
        para: res.data.para,
        id: res.data.id
      })
    })
  }
  render() { 
    const styles = {
      border: "1px solid black",
      borderRadius: "5px",
      padding : "10px",
  };
    return ( 
      <div>
        <Navbars />
        <section className="justify-center" >
            <Container>
            <Row>
            {this.state.users.map(user =>
            <Col xs={12} lg={3} md={5}>
            <Card border="danger" className="mt-3">
                <Card.Header>{user.name}</Card.Header>
                <Card.Body>
                <Card.Title>{user.email} </Card.Title>
                <Card.Text>
                    {user.para}
                </Card.Text>
                </Card.Body>
            </Card>
            </Col>
            )}
            </Row>
            </Container>
            </section>
        <Container>
        <Row>
        <Col>
        <Form className="my-3" style={styles}  onSubmit={(i) => {this.submit(i, this.state.id)}}>
            <Form.Group controlId="exampleForm.ControlInput1">
                <Form.Label>Heading</Form.Label>
                <Form.Control type="text" placeholder="Heading" onChange={(i) => {this.nameChange(i)}} value={this.state.name} />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="name@example.com" onChange={(i) => {this.emailChange(i)}} value={this.state.email}/>
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label>Example textarea</Form.Label>
                <Form.Control as="textarea" rows="3" onChange={(i) => {this.paraChange(i)}} value={this.state.para}/>
            </Form.Group>
            <Button variant="outline-danger">Submit</Button>
          </Form>
        </Col>
        <Col>
        <section className="mt-3">
        <h2>Table</h2>
            <Table striped bordered hover variant="dark">
            <thead>
                <tr>
                <th>#</th>
                <th>Heading</th>
                <th>Email</th>
                <th>Para</th>
                <th>#</th>
                <th>#</th>
                </tr>
            </thead>
            <tbody>
            {this.state.users.map(user => 
                <tr>
                <td>1</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.para} </td>
                <td><Button variant="outline-primary" className="ml-3" onClick={(i) => {this.update(user.id)}}>Edit</Button></td>
                <td><Button variant="outline-danger" className="ml-3" onClick={(i) => {this.delete(user.id)}}>Delete</Button></td>
                </tr>
            )}                   
            </tbody>
            </Table>

        </section>
        </Col>
        </Row>
        </Container>
      </div>
     );
  }
}
 
export default App;

