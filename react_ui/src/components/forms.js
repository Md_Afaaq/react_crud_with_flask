import React, { Component } from 'react'
import { Form, Button } from 'react-bootstrap';


class Forms extends Component {
    render() { 
        const styles = {
            border: "1px solid black",
            borderRadius: "5px",
            padding : "10px",
        };
        return ( 
            <section>
                <h2>From</h2>
                <Form className="my-3" style={styles}>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Heading</Form.Label>
                    <Form.Control type="text" placeholder="Heading" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="name@example.com" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Example textarea</Form.Label>
                    <Form.Control as="textarea" rows="3" />
                </Form.Group>
                <Button variant="outline-danger">Submit</Button>
                </Form>
            </section>
        );
    }
}
 
export default Forms;