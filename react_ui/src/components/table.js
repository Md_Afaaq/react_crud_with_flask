import React, { Component } from 'react'
import { Table, Button } from 'react-bootstrap';



function Tables(props) {

    return ( 
        <section className="mt-3">
        <h2>Table</h2>
            <Table striped bordered hover variant="dark">
            <thead>
                <tr>
                <th>#</th>
                <th>Heading</th>
                <th>Email</th>
                <th>Para</th>
                <th>#</th>
                <th>#</th>
                </tr>
            </thead>
            <tbody>
            {this.state.data.map(user => 
                <tr>
                <td>1</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.par} </td>
                <td><Button variant="outline-primary" className="ml-3">Edit</Button></td>
                <td><Button variant="outline-danger" className="ml-3">Delete</Button></td>
                </tr>
            )}                   
            </tbody>
            </Table>

        </section>
    );
}
 
export default Tables;