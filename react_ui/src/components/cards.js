import React, { Component } from 'react'
import { Card, Container, Row, Col } from 'react-bootstrap';


class Cards extends Component {
    render() { 
        return ( 
            <section className="justify-center" >
            <Container>
            <Row>
            <Col xs={12} lg={3} md={5}>
            <Card border="danger" className="mt-3">
                <Card.Header>Header</Card.Header>
                <Card.Body>
                <Card.Title>Card Title </Card.Title>
                <Card.Text>
                    Some quick example text to build on the card title and make up the bulk
                    of the card's content.
                </Card.Text>
                </Card.Body>
            </Card>
            </Col>
            </Row>
            </Container>
            </section>
    );
    }
}
 
export default Cards;
